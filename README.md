# OpenPDAC

OpenPDAC is an offline Eulerian-Lagrangian model for the simulation of volcanic fluids. The model solves the
conservation equations of mass, momentum and energy for a Eulerian-Eulerian compressible multiphase mixture of
gasses and fine (up to mm size) particles, and the Lagrangian transport equations for coarser solid particles. The solver is
based on the equations of the PDAC model, ported and optimized on the open-source OpenFOAM C++ toolbox for
Computational Fluid Dynamics (CFD).

## Documentation

## Developers

## License

## Copyright


